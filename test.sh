#!/bin/sh -x

set -e

rm -rf origin dev1 dev2 bamboo

#create the origin
mkdir origin
cd origin
git init
touch file
git add .
git commit -am "initial commit"
git config receive.denyCurrentBranch ignore
cd ..

git clone origin dev1
cd dev1
git checkout -b guru-test
touch test
git add .
git commit -am "guru-test"
git push -u origin guru-test
cd ..

git clone origin dev2
cd dev2
echo dev2 > file
git commit -am "master"
git push origin master
cd ..

git clone origin bamboo
cd bamboo
git checkout guru-test
TARGET_BRANCH=master ../pre-build.sh
cd ..

cd dev1
echo test2 > test
git commit -am "guru-test 2"
git push origin guru-test
cd ..

cd dev2
echo "dev2 again" > file
git commit -am "master 2"
git push origin master
cd ..

cd bamboo
BUILD_BRANCH=guru-test TARGET_BRANCH=master ../post-build.sh
cd ..

cd dev1
git pull --rebase
