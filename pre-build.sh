#!/bin/sh

set -e

if [[ "" == "$TARGET_BRANCH" ]]; then
	echo "Target branch not specified" 1>&2
	exit 1
fi

BUILD_BRANCH=$(git symbolic-ref -q HEAD)
BUILD_BRANCH=${BUILD_BRANCH##refs/heads/}
BUILD_BRANCH=${BUILD_BRANCH:-HEAD}

set +e
origin_exists=$(git remote show -n | grep origin)
set -e

if [[ "" == "$origin_exists" ]]; then
	git remote add origin git@bitbucket.org:dfabulich/example.git
fi

chmod 0600 $(dirname $0)/deploy_key

git config user.email "bamboo@example.com"
git config user.name "Atlassian Bamboo"
SSH_KEYFILE=deploy_key GIT_SSH=$(dirname $0)/ssh-with-custom-key git fetch origin

#we're on $BUILD_BRANCH
git rebase --preserve-merges origin/$TARGET_BRANCH
git branch -D $TARGET_BRANCH
git checkout $TARGET_BRANCH
git merge --ff-only $BUILD_BRANCH