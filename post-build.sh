#!/bin/sh

set -e

if [[ "" == "$TARGET_BRANCH" ]]; then
	echo "Target branch not specified" 1>&2
	exit 1
fi

if [[ "" == "$BUILD_BRANCH" ]]; then
	echo "Target branch not specified" 1>&2
	exit 1
fi

SSH_KEYFILE=deploy_key GIT_SSH=$(dirname $0)/ssh-with-custom-key git fetch origin

#clear out old bamboo-temp-branch, if any
set +e
git branch -D bamboo-temp-branch > /dev/null 2>&1
set -e

#if there are unbuilt changes on $BUILD_BRANCH, force pushing will destroy them
#so rebase origin/BUILD_BRANCH onto our BUILD_BRANCH
#then FF merge origin/BUILD_BRANCH into our BUILD_BRANCH
#This requires a temp branch, because you can't rebase remote branches directly

git checkout -b bamboo-temp-branch origin/$BUILD_BRANCH
git rebase --preserve-merges $BUILD_BRANCH

git checkout $BUILD_BRANCH
git merge --ff-only bamboo-temp-branch
git branch -D bamboo-temp-branch

#now pull on TARGET_BRANCH
git checkout $TARGET_BRANCH
GIT_EDITOR=true git merge --commit origin/$TARGET_BRANCH

#push everything, but force-push $BUILD_BRANCH with +
SSH_KEYFILE=deploy_key GIT_SSH=$(dirname $0)/ssh-with-custom-key git push origin $TARGET_BRANCH +$BUILD_BRANCH 

