#!/bin/sh -x

set -e
./pre-build-conflict-test.sh
./post-build-branch-conflict-test.sh
./post-build-master-conflict-test.sh
./test.sh
echo ALL TESTS PASS